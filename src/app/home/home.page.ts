import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  public weight: number;
  public gender: string;
  public time: number;
  public bottles: number;
  public promilles: number;
  public litres: number;
  public grams: number;
  public burning: number;
  public gramsLeft: number;

  constructor() {}

  ngOnInit() {

    this.weight = 80;
    this.time = 1;
    this.gender = 'm';
    this.bottles = 3;
  }

    public calculate() {
      const litres = this.bottles * 0.33;
      const grams = litres * 8 * 4.5;
      const burning = this.weight / 10;
      const gramsLeft = grams - (burning * this.time);

      if (this.gender === 'm') {
        this.promilles = gramsLeft / (this.weight * 0.7);
      }
    else {
      this.promilles = gramsLeft / (this.weight * 0.6);
    }
  }
}
